#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp> // this load in the sound buffers
#include <string>
#include <vector>
#include "AssetManager.h"
#include "SpriteObject.h"
#include "Player.h"
#include "AnimatingObject.h"
#include "Wall.h"
#include "Level.h"
#include <cstdlib> // we are using this to gen random Numbers
#include <time.h> // see above



int main()
{
	// Declare our SFML window, called gameWindow
	sf::RenderWindow gameWindow;
	// Set up the SFML window, passing the dimmensions and the window name
	gameWindow.create(sf::VideoMode::getDesktopMode(), "Maze By Scott Gilbert", sf::Style::Titlebar | sf::Style::Close);
	//gameWindow.create(sf::VideoMode::getDesktopMode(), "Maze By Scott Gilbert", sf::Style::Fullscreen);

	
	//++++++++++++++++++++++++++++++++Setup Section 

	sf::Sprite testSprite(AssetManager::RequestTexture("Assets/Graphics/PlayerStatic.png"));
	
	// This Set the player Start loc and make the player class 
	Player PlayerObject(sf::Vector2f(500, 500));

	// trying to setup anmation 
	AnimatingObject animatingObject(AssetManager::RequestTexture("Assets/Graphics/PlayerAnimation.png"),
		200,
		200,
		5.0f);
	//Removing test wall 
	//Wall WallObject(sf::Vector2f(600, 600));


	Level levelScreen;
	levelScreen.LoadLevel(1);



	// Game Clock

	// Create a time value to store the total time limit for our game
	sf::Time timeLimit = sf::seconds(60.0f);
	// Create a timer to store the time remaining for our game
	sf::Time timeRemaining = timeLimit;

	// Create a clock to track time passed each frame in the game
	sf::Clock gameClock;




	//++++++++++++++++++++++++++++++ Game Loop 

	while (gameWindow.isOpen())
	{
		sf::Event event;
		while (gameWindow.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				gameWindow.close();
		}

		
		// Get the time passed since the last frame and restart our game clock
		sf::Time frameTime = gameClock.restart();
		// Update our time remaining based on how much time passed last frame
		timeRemaining = timeRemaining - frameTime;

		//++++++++++++++++++++++++++++++++Update Section 

		PlayerObject.Input();


		PlayerObject.Update(frameTime);

		//PlayerObject.HandleSolidCollision(WallObject.GetHitbox());

		//++++++++++++++++++++++++++++++++Draw Section 
		gameWindow.clear();
		// Draw everything to the window
		PlayerObject.DrawTo(gameWindow);
		//WallObject.DrawTo(gameWindow);
		//animatingObject.DrawTo(gameWindow);
		gameWindow.display();
	}


	

	
	// End of Game Loop
	return 0;
	
}
