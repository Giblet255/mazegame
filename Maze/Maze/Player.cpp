#include "Player.h"
#include "AssetManager.h"
#include <cmath>





Player::Player(sf::Vector2f startingPos)
	: AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/PlayerAnimation3.png"),131,201,5.0f)
		, velocity(0.0f, 0.0f)
		, speed(300.0f)
	    , previousPosition(startingPos)
{
	sprite.setPosition(startingPos);
	//sprite.setScale(200, 200);

	AddClip("WalkDown",0,3);
	AddClip("WalkRight",4,7);
	AddClip("WalkUp",8,11);
	AddClip("WalkLeft",12,15);
	// Set player looking down
	PlayClip("WalkDown");
}

void Player::Input()
{
	// Player keybind input
	// Start by zeroing out player velocity
	velocity.x = 0.0f;
	velocity.y = 0.0f;
	bool hasInput = false;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		// Move player up
		velocity.y = -speed;
		if (!hasInput) 
		{
			PlayClip("WalkUp");
		}
		
		hasInput = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		// Move player left
		velocity.x = -speed;
		if (!hasInput)
		{
			PlayClip("WalkLeft");
		}

		hasInput = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		// Move player down
		velocity.y = speed;
		if (!hasInput)
		{
			PlayClip("WalkDown");
		}

		hasInput = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		// Move player right
		velocity.x = speed;
		if (!hasInput)
		{
			PlayClip("WalkRight");
		}

		hasInput = true;
	}

	// Added ArrowKeys 

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		// Move player up
		velocity.y = -speed;
		if (!hasInput)
		{
			PlayClip("WalkUp");
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		// Move player left
		velocity.x = -speed;
		if (!hasInput)
		{
			PlayClip("WalkLeft");
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		// Move player down
		velocity.y = speed;
		if (!hasInput)
		{
			PlayClip("WalkDown");
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		// Move player right
		velocity.x = speed;
		if (!hasInput)
		{
			PlayClip("WalkRight");
		}
	}




}

void Player::Update(sf::Time frameTime)
{
	// Calculate the new position
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();
	// this is get prevous pot you need this to make player jump back
	previousPosition = sprite.getPosition();

	// Move the player to the new position
	sprite.setPosition(newPosition);

	

	AnimatingObject::Update(frameTime);

}

void Player::HandleSolidCollision(sf::FloatRect otherHitbox)
{
	// Check if there is actually a collision happening
	bool isColliding = GetHitbox().intersects(otherHitbox);
	// If there is a collision...
	if (isColliding)
	{
		// TODO: Calculate the collision depth (overlap)
		sf::Vector2f depth = CalculateCollisionDepth(otherHitbox);
		// TODO: Determine which is smaller - the x or y overlap
		// Determine which is smaller - the x or y overlap
		sf::Vector2f newPosition = sprite.getPosition();
		if (std::abs(depth.x) < std::abs(depth.y))
		{
			// Calculate a new x coordinate such that the objects don't overlap
				newPosition.x -= depth.x;
		}
		else
		{
			// TODO: Calculate a new y coordinate such that the objects don't overlap
			newPosition.y -= depth.y;
		}

		// TODO: Move the sprite by the depth in whatever direction wassmaller		// Move the sprite by the depth in whatever direction was smaller
			sprite.setPosition(newPosition);
		
	}
}
